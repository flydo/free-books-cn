# 免费中文编程书籍索引

免费中文编程书籍索引。个人收集，欢迎投稿。

- GitHub 上流行的其它中文编程书籍索引 [索引一](https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-zh.md)、 [索引二](https://github.com/justjavac/free-programming-books-zh_CN/blob/main/README.md)

## 中文文档平台

- 菜鸟教程：https://www.runoob.com/
- 书栈网：https://www.bookstack.cn/
- 看云网：https://www.kancloud.cn/explore
- 网道（偏前端）：https://wangdoc.com/
- 廖雪峰的官方网站：https://www.liaoxuefeng.com/
- 印记中文（前端）：https://docschina.org/
- LearnKu：https://learnku.com/
- Rust 官方文档中文教程：https://www.rustwiki.org.cn/
- 太空编程（前端）：https://spacexcode.com/
- Geek 文档网站：https://geekdaxue.co/

## 文档翻译团队

- **[ApacheCN](https://www.apachecn.org)**： https://github.com/apachecn

---

## 目录

- [操作系统与应用](#操作系统与应用)

  - [OpenHarmony & HarmonyOS](#openharmony)

- [编程语言类](#编程语言类)

  - [Go](#go)
  - [Java](#java)
  - [JavaScript](#javascript)
  - [JSON](#json)
  - [Lua](#lua)
  - [PHP](#php)
  - [Python](#python)
  - [Rust](#rust)
  - [Shell](#shell)
  - [TypeScript](#typescript)
  - [Vim](#vim)

- [数据库与缓存类](#数据库与缓存类)

  - [PostgreSQL](#postgresql)
  - [Redis](#redis)
  - [Valkey](#valkey)

- 其它

  - [云原生](#云原生)
  - [量化交易](#量化交易)
  - [图书](#图书)
  - [图形图像](#图形图像)

---

## 操作系统与应用

### OpenHarmony

- [ArkUI实战](https://gitee.com/ark-ui/arkui_in_action) / [BOOK](https://www.arkui.club/)

---

## 编程语言类

### [Cangjie (仓颉编程语言)](https://cangjie-lang.cn/)

### [Go](https://go.dev)

- [Go 编程基础](https://github.com/Unknwon/go-fundamental-programming) （作者：无闻）
- [Go 入门指南](https://github.com/Unknwon/the-way-to-go_ZH_CN)（作者：无闻）
- [Go Web 编程](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/preface.md) （作者：谢孟军）
- [Go 语言标准库](https://github.com/polaris1119/The-Golang-Standard-Library-by-Example) （译者：Go 语言中文网）
- [GO 命令教程](https://github.com/hyper0x/go_command_tutorial) （作者：hyper0x）
- [Go 语言实战: 编写可维护 Go 语言代码建议](https://github.com/llitfkitfk/go-best-practice) （作者：llitfkitfk）
- [Golang 系列教程](https://github.com/Tinywan/golang-tutorial) _**[原书](https://golangbot.com/)**_ (译者：Tinywan，未译全)
- [Go RPC 开发指南](https://github.com/smallnest/go-rpc-programming-guide) / [GitBook](https://smallnest.gitbooks.io/go-rpc-programming-guide/) / [rpcx.io](https://doc.rpcx.io/) （作者：smallnest）
- [Go 语言设计模式](https://github.com/senghoo/golang-design-pattern) （作者：senghoo）
- [Go 语言四十二章经](https://github.com/ffhelicopter/Go42) （作者：李骁）
- [Go 语言圣经](https://github.com/golang-china/gopl-zh) / [BOOK1](https://books.studygolang.com/gopl-zh/) / [BOOK2](https://gopl-zh.github.io/) / [BOOK3](https://golang-china.github.io/gopl-zh/) _**[原书](http://gopl.io/)**_ （译者：柴树杉等）
- [Go 语言定制指南](https://github.com/chai2010/go-ast-book) / [BOOK](https://chai2010.cn/go-ast-book/) （作者：柴树杉等）
- [Go 语言高级编程](https://github.com/chai2010/advanced-go-programming-book) / [GitBook](https://chai2010.gitbooks.io/advanced-go-programming-book/content/) / [BOOK](https://chai2010.cn/advanced-go-programming-book/) （作者：柴树杉等）
- [Go2 编程指南](https://github.com/golang-china/go2-book) / [GitBook](https://golang-china.github.io/go2-book/) （作者：柴树杉）
- [Learn Go with Tests](https://studygolang.gitbook.io/learn-go-with-tests) / [GitBook](https://studygolang.gitbook.io/learn-go-with-tests) _**[原书](https://github.com/quii/learn-go-with-tests)**_ （译者：Donng）
- [Go 语言高性能编程](https://github.com/geektutu/high-performance-go) / [BOOK](https://geektutu.com/post/high-performance-go.html) （作者：geektutu）
- [Go 语言必知必会](https://github.com/duanbiaowu/go-examples-for-beginners) / [BOOK](https://golang.dbwu.tech/) （作者：duanbiaowu）
- [Golang 面试题搜集](https://github.com/lifei6671/interview-go) （作者：lifei6671）
- > 停更
  - [Go 语言博客实践](https://github.com/achun/Go-Blog-In-Action)
  - [golang runtime 源码分析](https://github.com/sheepbao/golang_runtime_reading)
  - [深入解析 Go](https://github.com/tiancaiamao/go-internals)
  - [学习 Go 语言](https://github.com/mikespook/Learning-Go-zh-cn)

### [Java](https://www.oracle.com/java/)

- [On Java 8](https://github.com/LingCoder/OnJava8/tree/1ef7ec48e492862300e667e24c245e9b3a5ccd98)
  > 非官方On Java 8译本  
  > 因版权方已上架中文译本，此文档需从 commit 中查阅。

### JavaScript

- [MDN - Web 开发技术](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript) （作者: MDN Web Docs 社区）
- [MDN - 学习 Web 开发](https://developer.mozilla.org/zh-CN/docs/Learn/JavaScript) （作者: MDN Web Docs 社区）
- [ECMAScript 6 入门教程](http://es6.ruanyifeng.com/) [Mirror](https://wangdoc.com/es6/) （作者: 阮一峰）
- [ECMAScript 5.1 中文版](https://www.w3.org/html/ig/zh/wiki/ES5) / [Mirror1](http://yanhaijing.com/es5) _**[原书](https://262.ecma-international.org/5.1/)**_
- [现代 Javascript 教程](https://github.com/javascript-tutorial/zh.javascript.info) / [BOOK](https://zh.javascript.info/)
- [JavaScript 教程](https://github.com/wangdoc/javascript-tutorial) / [BOOK](https://wangdoc.com/javascript/)
- [JavaScript 全栈教程](https://www.liaoxuefeng.com/wiki/1022910821149312) (作者: 廖雪峰)
- > **【停更】**
  - [JavaScript Promise 迷你书](https://github.com/liubin/promises-book/) / [BOOK](http://liubin.github.io/promises-book/) _**[原书](https://github.com/azu/promises-book)**_
  - [学用 JavaScript 设计模式](http://www.oschina.net/translate/learning-javascript-design-patterns) _**[原书](https://www.patterns.dev/posts)**_ (译者: 开源中国)
  - [深入理解 JavaScript 系列](http://www.cnblogs.com/TomXu/archive/2011/12/15/2288411.html) （整理：博客园作者）
- [**React**](https://react.dev/)
  - **[React 中文官方网站](https://zh-hans.react.dev/)**
  - [React 入门](https://developer.mozilla.org/zh-CN/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/React_getting_started) （作者: MDN Web Docs 社区）
  - [React 学习之道](https://github.com/the-road-to-learn-react/the-road-to-learn-react-chinese)
- [**Vue**](https://vuejs.org/)
  - **[Vue.js 中文官方网站](https://cn.vuejs.org/)**
  - [awesome-vue](https://github.com/vuejs/awesome-vue)
  - [逐行剖析 Vue.js 源码](https://github.com/NLRX-WJC/Learn-Vue-Source-Code) / [BOOK](https://nlrx-wjc.github.io/Learn-Vue-Source-Code/) （作者：NLRX-WJC）
- [**Angular**](https://angular.dev/)
  - **[Angular 中文官方网站](https://angular.cn)**
- [**Deno**](https://deno.com/)
  - > 停更
    - [Deno 钻研之术](https://github.com/hylerrix/deno-tutorial) / [BOOK](https://deno-tutorial.js.org/)
    - [Deno 中文手册](https://github.com/denocn/deno_docs) [BOOK](https://manual.deno.js.cn/introduction)
    - [Deno 进阶开发笔记](https://github.com/chenshenhai/deno_note)
    - [awesome-deno-cn](https://github.com/hylerrix/awesome-deno-cn)
- **其它**
  - [Chrome 扩展及应用开发](https://www.ituring.com.cn/book/1421)

### JSON
  - [Google JSON 风格指南](https://github.com/darcyliu/google-styleguide/blob/master/JSONStyleGuide.md) / _**[原书](https://google.github.io/styleguide/jsoncstyleguide.xml)**_

### [Lua](https://www.lua.org/)

- [Lua 5.3 参考手册 中文翻译](http://cloudwu.github.io/lua53doc/)
- [Lua 编程入门](https://github.com/andycai/luaprimer)

### [PHP](https://www.php.net/)

- **[PHP 官方中文手册](http://php.net/manual/zh/)**
- [PHP 之道](https://github.com/laravel-china/php-the-right-way) / [BOOK](https://learnku.com/docs/php-the-right-way/PHP8.0) _**[原书](https://github.com/codeguy/php-the-right-way)**_
- [Laravel 中文文档](https://learnku.com/docs/laravel/)
- [Yii 中文官网](https://www.yiichina.com/)
- [Swoole 中文官网](https://www.swoole.com/)
- [awesome-swoole](https://github.com/swoole/awesome-swoole)
- [Lumen 中文文档](https://learnku.com/docs/lumen/)
- [PHPUnit 中文文档](https://phpunit.de/manual/current/zh_cn/)

### [Python](https://python.org/)

- [Python 官方中文文档](https://docs.python.org/zh-cn/3/contents.html)
- [PyQt 中文教程](https://github.com/maicss/PyQt-Chinese-tutorial) / [BOOK](https://maicss.gitbook.io/pyqt-chinese-tutoral/) （作者：maicss）
- [Python3 网络爬虫开发实战](https://python3webspider.cuiqingcai.com/)
- [Python 新手入门教程](https://www.liaoxuefeng.com/wiki/1016959663602400) （作者：廖雪峰）

### [Rust](https://rust-lang.org/)

- [Rust 程序设计语言](https://github.com/rust-lang/book) / [rustwiki 译本](https://github.com/rust-lang-cn/book-cn)&[BOOK](https://rustwiki.org/zh-CN/book/) / [KaiserY 译本](https://github.com/KaiserY/trpl-zh-cn)&[GitBook](https://kaisery.github.io/trpl-zh-cn/)
- [awesome-rust](https://github.com/rust-unofficial/awesome-rust)
- [速读Rust 权威指南（知乎专栏）](https://www.zhihu.com/column/c_1376302142102335488)
- [The Rustonomicon](https://github.com/rust-lang/nomicon) / [rustwiki 译本](https://github.com/rust-lang-cn/nomicon-zh-Hans)&[BOOK](https://nomicon.purewhite.io/) / [tjxing 译本](https://github.com/tjxing/rustonomicon_zh-CN)
- [The Cargo Book](https://github.com/rust-lang/cargo) / [rustwiki 译本](https://github.com/rust-lang-cn/cargo-cn)&[BOOK](https://rustwiki.org/zh-CN/cargo/) / [chinanf-boy 译本](https://github.com/chinanf-boy/cargo-book-zh)&[BOOK](http://llever.com/cargo-book-zh/)
- [Rust Cargo.toml 详解](https://zhuanlan.zhihu.com/p/472961111)
- [Rust By Example](https://github.com/rust-lang/rust-by-example) / [rustwiki 译本](https://github.com/rust-lang-cn/rust-by-example-cn)&[BOOK](https://rustwiki.org/zh-CN/rust-by-example/)
- [Rust Cookbook](https://github.com/rust-lang-nursery/rust-cookbook) / [chinanf-boy 译本](https://github.com/chinanf-boy/rust-cookbook-zh)&[BOOK](https://llever.com/rust-cookbook-zh/)
- [Rust Style Guide](https://github.com/rust-lang/rust/tree/master/src/doc/style-guide/src) / [rustwiki 译本](https://github.com/rust-lang-cn/style-guide-cn)&[BOOK](https://rustwiki.org/zh-CN/style-guide/) / [其它译本](https://github.com/Rust-Coding-Guidelines/rust-coding-guidelines-zh)&[BOOK](https://rust-coding-guidelines.github.io/rust-coding-guidelines-zh/)
- [Rust 语言圣经](https://github.com/sunface/rust-course) / [BOOK](https://course.rs/)
- [Rusty Book（锈书）](https://github.com/rustlang-cn/rusty-book) / [BOOK](https://rusty.rs/)
- [Rust 标准库](https://github.com/Warrenren/inside-rust-std-library)
- [Rust 语言实战](https://github.com/sunface/rust-by-practice) / [BOOK](https://zh.practice.rs/)
- [Rust 入门秘籍](https://rust-book.junmajinlong.com/)
- [Comprehensive Rust](https://github.com/google/comprehensive-rust) / [BOOK](https://google.github.io/comprehensive-rust/zh-CN/)
- [pretzelhammer rust-blog](https://github.com/pretzelhammer/rust-blog)
- [Rust 编码规范](https://github.com/Rust-Coding-Guidelines/rust-coding-guidelines-zh) / [BOOK](https://rust-coding-guidelines.github.io/rust-coding-guidelines-zh/)

### [Shell](https://www.gnu.org/software/bash/manual/bash.html)

- [Linux 命令搜索引擎](https://github.com/jaywcjlove/linux-command) / [BOOK](https://wangchujiang.com/linux-command/)
- [Linux 工具快速教程](https://github.com/me115/linuxtools_rst) / [BOOK](https://linuxtools-rst.readthedocs.io/zh_CN/latest/)
- [shell 十三问](https://github.com/wzb56/13_questions_of_shell)
- [Shell 编程范例](https://github.com/tinyclub/open-shell-book) / [GitBook](http://tinylab.gitbooks.io/shellbook)

### [TypeScript](https://www.typescriptlang.org/)

- [TypeScript 使用手册](https://github.com/zhongsp/TypeScript) / [GitBook](https://zhongsp.gitbooks.io/typescript-handbook/content/) / _**[原书](https://www.typescriptlang.org/)**_ （译者：zhongsp）
- [TypeScript 教程](https://github.com/wangdoc/typescript-tutorial) / [BOOK](https://wangdoc.com/typescript/) （作者：阮一峰）

### Vim

- [VIM 参考手册](https://github.com/yianwillis/vimcdoc) / [BOOK](https://yianwillis.github.io/vimcdoc/doc/)
- [Vim 从入门到精通](https://github.com/wsdjeg/vim-galore-zh_cn) （作者： wsdjeg）
- [Vim 插件开发指南](https://github.com/wsdjeg/vim-plugin-dev-guide) （作者： wsdjeg）
- [vim 实操教程](https://github.com/dofy/learn-vim) / [GitBook](https://dofy.gitbook.io/learn-vim/)
- [Vim Practice](https://github.com/oldratlee/vim-practice)
- [VimL 语言编程指北路](https://github.com/lymslive/vimllearn)
- [笨方法学 Vimscript](https://github.com/isayme/learnvimscriptthehardway-cn) / [BOOK](https://huhuang03.gitbooks.io/learnvimscriptthehardway/content/) _**[原书](https://learnvimscriptthehardway.stevelosh.com/)**_

---

## 数据库与缓存类

### [PostgreSQL](https://www.postgresql.org/)

- [PostgreSQL 14 简体中文文档](http://www.postgres.cn/docs/14/index.html)
- [PostgreSQL 繁体中文文档](https://docs.postgresql.tw/)
- [Redrock Postgres 文档](https://doc.rockdata.net/zh-cn/)

### [Redis](https://redis.io/)

- [Redis 中文文档](https://wizardforcel.gitbooks.io/redis-doc/content/) （译者：黄健宏）

## [Valkey](http://valkey.io/)

---

## 其它

### 云原生

- [Kubernetes 指南](https://github.com/feiskyer/kubernetes-handbook) / [BOOK](https://kubernetes.feisky.xyz/)
- [Kubernetes中文指南/云原生应用架构实战手册](https://github.com/rootsongjc/kubernetes-handbook) / [BOOK](https://jimmysong.io/book/kubernetes-handbook/)
- [从 Docker 到 Kubernetes 进阶（停更）](https://github.com/cnych/kubernetes-learning) / [BOOK](https://www.qikqiak.com/k8s-book/)
- [和我一步步部署 kubernetes 集群（停更）](https://github.com/opsnull/follow-me-install-kubernetes-cluster) / [BOOK](https://k8s-install.opsnull.com/)
- [Docker — 从入门到实践](https://github.com/yeasy/docker_practice) / [BOOK](https://vuepress.mirror.docker-practice.com/) / [GitBook](https://yeasy.gitbook.io/docker_practice)

### 量化交易

- [量化交易译文集](https://github.com/OpenDocCN/freelearn-quant-zh) / [BOOK](https://flq.flygon.net/)
- [Python 量化交易教程](https://wizardforcel.gitbooks.io/python-quant-uqer/content/)

### 图书

- [高并发的哲学原理](https://github.com/johnlui/PPHC) / [BOOK](https://pphc.lvwenhan.com/)

### 图形图像

- [学习 wgpu](https://github.com/jinleili/learn-wgpu-zh) / [BOOK](https://jinleili.github.io/learn-wgpu-zh/) / [原书](https://github.com/sotrh/learn-wgpu)

---

[**查看目录**](#目录)

## Review

> 已审阅: 全部已审阅  
> 待审阅:  
> 审阅至:

## 仓库镜像

- https://git.jetsung.com/flydo/free-books-cn
- https://framagit.org/flydo/free-books-cn
- https://gitcode.com/flydo/free-books-cn
